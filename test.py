import unittest
import japan


class TestDrawMethods(unittest.TestCase):

    def test_create_canvas(self):
        answer = [['-', '-', '-', '-'],
                  ['|', ' ', ' ', '|'],
                  ['|', ' ', ' ', '|'],
                  ['|', ' ', ' ', '|'],
                  ['-', '-', '-', '-']]
        result = japan.set_canvas(2, 3, "-", "|", " ")
        self.assertEqual(answer, result)

    def test_draw_line(self):
        answer = [['-', '-', '-', '-', '-'],
                  ['|', ' ', 'X', 'X', '|'],
                  ['|', 'X', 'X', 'X', '|'],
                  ['|', 'X', ' ', ' ', '|'],
                  ['-', '-', '-', '-', '-']]
        result = japan.set_canvas(3, 3, "-", "|", " ")
        japan.draw_line(result, 1, 2, 3, 2, "X")
        japan.draw_line(result, 2, 2, 2, 1, "X")
        japan.draw_line(result, 1, 3, 3, 1, "X")
        self.assertEqual(answer, result)

    def test_draw_rectangle(self):
        answer = [['-', '-', '-', '-', '-', '-', '-'],
                  ['|', ' ', ' ', ' ', ' ', ' ', '|'],
                  ['|', ' ', 'x', 'x', 'x', ' ', '|'],
                  ['|', ' ', 'x', ' ', 'x', ' ', '|'],
                  ['|', ' ', 'x', 'x', 'x', ' ', '|'],
                  ['|', ' ', ' ', ' ', ' ', ' ', '|'],
                  ['-', '-', '-', '-', '-', '-', '-']]
        result = japan.set_canvas(5, 5, "-", "|", " ")
        japan.draw_rectangle(result, 2, 2, 4, 4, "x", "x")
        self.assertEqual(answer, result)


    def test_draw_line_by_rectangle(self):
        answer = [['-', '-', '-', '-', '-', '-', '-'],
                  ['|', ' ', 'x', ' ', ' ', ' ', '|'],
                  ['|', ' ', 'x', 'x', 'x', 'x', '|'],
                  ['|', ' ', 'x', ' ', ' ', ' ', '|'],
                  ['|', ' ', ' ', ' ', ' ', ' ', '|'],
                  ['|', ' ', ' ', ' ', ' ', ' ', '|'],
                  ['-', '-', '-', '-', '-', '-', '-']]
        result = japan.set_canvas(5, 5, "-", "|", " ")
        japan.draw_rectangle(result, 2, 2, 5, 2, "x", "x")
        japan.draw_rectangle(result, 2, 1, 2, 3, "x", "x")
        self.assertEqual(answer, result)


    def test_draw_circle(self):
        answer = [['-', '-', '-', '-', '-', '-', '-'],
                  ['|', ' ', 'x', 'x', ' ', ' ', '|'],
                  ['|', 'x', ' ', ' ', 'x', ' ', '|'],
                  ['|', 'x', ' ', ' ', 'x', ' ', '|'],
                  ['|', ' ', 'x', 'x', ' ', ' ', '|'],
                  ['|', ' ', ' ', ' ', ' ', ' ', '|'],
                  ['-', '-', '-', '-', '-', '-', '-']]
        result = japan.set_canvas(5, 5, "-", "|", " ")
        japan.draw_circle(result, 1, 1, 4, 4, "x")
        self.assertEqual(answer, result)


    def test_draw_flag(self):
        answer = "##############\n#            #\n#            #\n#     xx     #\n#    xoox    #\n#    xoox    #\n#     xx     #\n#            #\n#            #\n##############\n"
        result = japan.draw_flag(4, "#","#"," ","x","o")
        self.assertEqual(answer, result)


    def test_paint_over(self):
        answer = [['-', '-', '-', '-', '-'],
                  ['|', ' ', 'X', ' ', '|'],
                  ['|', 'X', 'X', 'X', '|'],
                  ['|', '^', '^', '^', '|'],
                  ['-', '-', '-', '-', '-']]

        result = japan.set_canvas(3, 3, "-", "|", " ")
        japan.draw_line(result, 1, 2, 3, 2, "X")
        japan.draw_line(result, 2, 2, 2, 1, "X")
        japan.paint_over(result, 1, 3, "^")
        self.assertEqual(answer, result)


    def test_fill_line(self):
        answer = [['-', '-', '-', '-', '-'],
                  ['|', ' ', ' ', ' ', '|'],
                  ['|', '^', '^', '^', '|'],
                  ['|', ' ', ' ', ' ', '|'],
                  ['-', '-', '-', '-', '-']]

        result = japan.set_canvas(3, 3, "-", "|", " ")
        japan.draw_line(result, 1, 2, 3, 2, "x")
        japan.paint_over(result, 1, 2, "^")
        self.assertEqual(answer, result)


if __name__ == '__main__':
    unittest.main()