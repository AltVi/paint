#!/usr/bin/python
# -*- coding: utf-8 -*-
"""This is console version of paint
 *constants aviable:
    - LOG_FORMAT - format of logging

    - LOGGER - object of log

    - TERMINAL_MESSAGE - log for terminal

    - ADRESS_LOG - path to log file

    - CANVAS_VERTICAL_SYMBOL - the character that is
    used to draw vertical lines for canvas

    - CANVAS_HORIZONTAL_SYMBOL - the character that
    is used to draw horizontal lines for canvas

    - LINE_SYMBOL - the character that is used to draw lines

    - FLAG_SYMBOL - the character that is used to draw the flag border

    - FILLING_SYMBOL - the character that is used to fill the shapes

    - SPACE - character that fills empty space
"""


import os
import logging
import argparse
from queue import Queue


LOG_FORMAT = "Time: %(asctime)s  Type: [%(levelname)s] Message: %(message)s"
LOGGER = logging.getLogger(__name__)
TERMINAL_MESSAGE = logging.StreamHandler()
ADRESS_LOG = os.path.join(os.path.dirname(__file__), "log.txt")

CANVAS_VERTICAL_SYMBOL = "|"
CANVAS_HORIZONTAL_SYMBOL = "-"
LINE_SYMBOL = "X"
FLAG_SYMBOL = "#"
FILLING_SYMBOL = "o"
SPACE = " "


def setup_log(adress_log=ADRESS_LOG, log_level="DEBUG", silent=False):
    """Function for redefining the log"""
    if not silent:
        LOGGER.addHandler(TERMINAL_MESSAGE)
    try:
        LOGGER.setLevel(log_level)
        logging.basicConfig(filename=adress_log, format=LOG_FORMAT)
        return True
    except:
        return False


def draw_line(canvas, x1, y1, x2, y2, symbol=LINE_SYMBOL):
    """includes 3 modes: horizontal, vertical and diagonal line drawing"""
    min_x = min(x1, x2)
    max_x = max(x1, x2)
    min_y = min(y1, y2)
    max_y = max(y1, y2)
    if x1 == x2:
        for y in range(min_y, max_y + 1):
            canvas[y][x1] = symbol
    elif y1 == y2:
        for x in range(min_x, max_x + 1):
            canvas[y1][x] = symbol
    elif abs(x1 - x2) == abs(y1 - y2):
        right_direction = False       #\
        if (x1 > x2 and y1 > y2) or (x1 < x2 and y1 < y2):
            right_direction = True    #/
        for _ in range(min_y, max_y + 1):
            if right_direction:
                canvas[min_y][min_x] = symbol
                min_x += 1
            else:
                canvas[min_y][max_x] = symbol
                max_x -= 1
            min_y += 1
    return canvas


def draw_rectangle(canvas, x1, y1, x2, y2,
                   hsymbol=LINE_SYMBOL, vsymbol=LINE_SYMBOL):
    """For drawing, specify the coordinates of 2 opposite vertices.2 2 5 2"""
    draw_line(canvas, x1, y1, x1, y2, vsymbol)
    draw_line(canvas, x2, y1, x2, y2, vsymbol)
    draw_line(canvas, x1, y1, x2, y1, hsymbol)
    draw_line(canvas, x1, y2, x2, y2, hsymbol)
    return canvas


def draw_circle(canvas, x1, y1, x2, y2, symbol=LINE_SYMBOL):
    """To draw, specify the coordinates of 2 opposite vertices
    of the SQUARE, which will contain a circle"""
    delta_x = int(abs(x1 + x2) / 2)
    delta_y = int(abs(y1 + y2) / 2)
    draw_line(canvas, delta_x + 1, y1, x2, delta_y, symbol)
    draw_line(canvas, x2, delta_y + 1, delta_x + 1, y2, symbol)
    draw_line(canvas, delta_x, y2, x1, delta_y + 1, symbol)
    draw_line(canvas, x1, delta_y, delta_x, y1, symbol)
    return canvas


def set_canvas(x0, y0, hsymbol=CANVAS_HORIZONTAL_SYMBOL,
               vsymbol=CANVAS_VERTICAL_SYMBOL, space=SPACE):
    """sets the field for drawing"""
    canvas = [[space for x in range(x0 + 2)] for y in range(y0 + 2)]
    draw_rectangle(canvas, 0, 0, x0 + 1, y0 + 1, hsymbol, vsymbol)
    return canvas


def paint_over(canvas, x, y, symbol=FILLING_SYMBOL):
    """Colors the selected shape. You can also repaint the lines"""
    source = canvas[y][x]
    canvas[y][x] = symbol
    q = Queue()
    q.put([x, y])
    while not q.empty():
        x, y = q.get()
        if canvas[y][x + 1] == source:
            canvas[y][x + 1] = symbol
            q.put([x + 1, y])
        if canvas[y + 1][x] == source:
            canvas[y + 1][x] = symbol
            q.put([x, y + 1])
        if canvas[y][x - 1] == source:
            canvas[y][x - 1] = symbol
            q.put([x - 1, y])
        if canvas[y - 1][x] == source:
            canvas[y - 1][x] = symbol
            q.put([x, y - 1])
    return canvas


def print_canvas(canvas):
    """Nothing special"""
    for line in canvas:
        yield "".join(line)


def draw_flag(N, hcanvas=FLAG_SYMBOL, vsymbol=FLAG_SYMBOL, space=SPACE,
              lsymbol=LINE_SYMBOL, psymbol=FILLING_SYMBOL):
    """Draws a flag. Available modes - Japan"""
    message = ""
    if N % 2 == 0:
        canvas = set_canvas(N * 3, N * 2, hcanvas, vsymbol, space)
        draw_circle(canvas, N + 1, N // 2 + 1, 2 * N, N // 2 * 3, lsymbol)
        if N != 2:
            paint_over(canvas, N * 3 // 2, N, psymbol)
        for line in print_canvas(canvas):
            message += "".join(line) + "\n"
        return message
    else:
        raise Exception("ArgumentError")


if __name__ == "__main__":
    N = 0
    silent = False
    message = ""

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("number", nargs=1,
                            help="Size of canvas: 3*N X 2*N", type=int)
        parser.add_argument("-s", "--silent", help="Disables logging to the console",
                            action="store_true")
        args = parser.parse_args()

        if args.number:
            N = args.number[0]
        if args.silent:
            silent = True
        setup_log(silent=silent)
    except:
        LOGGER.log(40, "Something wrong with parametres")
        N = int(input("N="))

    try:
        message = draw_flag(N)
        LOGGER.log(10, "Flag created")
        print(message)
    except:
        LOGGER.log(40, "Wrong N. N must be even and > 0")